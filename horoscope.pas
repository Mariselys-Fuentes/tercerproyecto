program ver_horoscope(input,output);
uses crt;
var 
	opc:integer;
	dia: array [0..1] of integer;
	mes: array [0..1] of integer;
	pred:integer;
	prediccion:array[0..11,0..4] of string;
	nombre:string;
	

procedure acuario;
begin
	prediccion[0,0]:='Cree en ti, deja de menospreciarte y subestimarte, eres tan capaz como cualquier otra persona, o incluso mas ';
	prediccion[0,1]:='Tus habilidades en el trabajo estan empazando a llamar la atencion de uno de tus superiores, este es tu momento de demostrar todo de lo que eres capaz';
	prediccion[0,2]:='Tu salud va muy bien ultimamente, recuerda ir periodicamente al odontologo y tomar mucha agua';
	prediccion[0,3]:='Los astros te tienen preparado un reencuentro con un amor del pasado, dale una segunda oportunidad y llovera la felicidad en tu vida';
	prediccion[0,4]:='Una de tus mascotas ha estado algo mal del estomago ultimamente, llevala al veterinario para determinar de una vez por todas que lo tiene mal';
	writeln(prediccion[0,pred]);

end;

procedure piscis;
begin
	prediccion[1,0]:='Te sientes vacio y cansado ultimamente, trata de centrar tu atencion en el trabjo, asi lograras distraerte un poco';
	prediccion[1,1]:='Ultimamente sientes que la relacion con alguien al que le tienes cari;o se esta tornando perjudicial para ti, alejate un poco de esta persona';
	prediccion[1,2]:='Hace unos dias recibiste una noticia que en el momento no te habia hecho gracia, pero ahora las cosas han cambiado';
	prediccion[1,3]:='Sigue trabajando duro, pronto veras los frutos de ese esfuerzo. No olvides que llamar a tus padres de vez en cuando';
	prediccion[1,4]:='Uno de tus padres te extra;a mucho, comunicate con ellos de vez en cuando para que sientan que incluso en la distancia estan contigo';
	writeln(prediccion[1,pred]);
end;	
	
procedure aries;
begin
	prediccion[2,0]:='Las bendiciones lloveran sobre ti en este mes, no lo desaproveches, toma cada una de las oprotunidades que se te presentan, te podran cambiar la vida';
	prediccion[2,1]:='Estas un poco mal de salud, presentas algun tema relacionado con alergias, tratalo con tiempo, no lo descuides';
	prediccion[2,2]:='Hoy es un excelente dia trabajar duro y disfrutar de los frutos de este trabajo duro';
	prediccion[2,3]:='Economicamente se presentara una oportunidad de negocio a la que no podras decirle que no, no la desaproveches';
	prediccion[2,4]:='En el amor hay alguien especial que no deja de pensar en ti, si no es conrrespondido hazle saber, pero si lo date la oportunidad';
	writeln(prediccion[2,pred]);
end;	

procedure tauro;
begin
	prediccion[3,0]:='El dia de hoy es necesario que recuerdes que los problemas son transitorios y todo siempre tiene una solucion, tomate un respiro y analiza bien las cosas';
	prediccion[3,1]:='Si vienes presentando inconvenientes en el trabajo o los estudios, los vas a superar, la semana venidera te abrira muchas puertas, mantenete atento';
	prediccion[3,2]:='En el amor has estado un poco solitario los ultimos tiempos, la pandemia a tenido repercusiones graves en tus relaciones, tranquilo, todo mejorara a partir de ahora';
	prediccion[3,3]:='Te encuentras perfectamente de salud, pero recuerda no descidar la dieta y los ejercicios, no pierdas el enfoque';
	prediccion[3,4]:='Animicamente trata de tomarte las cosas a la ligera, no te preocupes tanto. Tomate un dia para ti, sal a pasear o has algo que te guste';
	writeln(prediccion[3,pred]);
end;
	
procedure geminis;
begin
	prediccion[4,0]:='Es tiempo de iniciar algun proyecto nuevo que amplie tus horizontes economicos. Si te encuentras en medio de problemas de trabajo recibiras noticias positivas.';
	prediccion[4,1]:='Analiza todas las ofertas si te encuentras en un proceso de compra y venta. No hagas caso de vendedores que tratan de que compres cosas que no necesitas';
	prediccion[4,2]:='Un amigo cercano tiene un problema y necesita de tu ayuda y apoyo incondicional, no dudes en tenderle la mano a quien lo necesita.';
	prediccion[4,3]:='Se acerca una fecha especial para ti, puede ser un cumplea;os o un aniversario, no olvides de celebrar y disfrutar los buenos momentos';
	prediccion[4,4]:='Cada dia que pasa tus metas se van haciendo mas cercanas, no dejes de trabajar y esforzarte, falta poco.';
    writeln(prediccion[4,pred]);
end;

procedure cancer;
begin
	prediccion[5,0]:='En el dinero te va bien, pero hay cierta tendencia al descontrol, evitalo. Te sentiras con mucha satisfaccion en todo lo que hagas, te saldran las cosas bien.';
	prediccion[5,1]:='Necesitas renovarte o encontrar algo nuevo que te motive en lo laboral. No quieres amor en este momento, vas a aparcar un poco tus asuntos sentimentales y te dedicaras a otras cosas.';
	prediccion[5,2]:='Estas fenomenal y vas a dedicar mas tiempo a relajarte y a divertirte. Dejaras atras los miedos y te lanzaras a la conquista de lo que quieres.';
	prediccion[5,3]:='Tus asuntos emocionales estan negativamente influidos por los astros, paciencia. No te centres en lo que no puedes hacer, y piensa en lo que sí puedes.';
	prediccion[5,4]:='Cualquier molestia antigua empezara a desaparecer o a mejorar este dia. Vas a enfocar la vida con mas animo y mas alegria, y de salud estaras muy bien. ';
	writeln(prediccion[5,pred]);
end;

procedure leo;
begin
	prediccion[6,0]:='Tendras muchas satisfacciones durante esta época en el trabajo, te animaras. Procura avanzar todo lo que puedas en lo laboral, pronto vendra lo creativo.';
	prediccion[6,1]:='La falta de tiempo te puede hacer descuidar asuntos importantes, evitalo. Aprovecha este periodo para tomar la iniciativa con alguien que te gusta, no dejes que el miedo te domine.';
	prediccion[6,2]:='Podrias llevarte una sorpresa agradable con alguien del signo de Tauro. Un buen amigo te dara un consejo o sugerencia muy acertada, prestale atencion';
	prediccion[6,3]:='En el amor no dependas demasiado de la opinion de los demas, la seguridad esta en ti. Necesitas mas atenciones y cuidados, no dejes de lado tu estado fisico.';
	prediccion[6,4]:='Estas con un poco de agobio, pero es mas mental que fisico, trata de calmarte. Necesitaras una buena dosis de descanso y de ocio para recuperar el humor y la salud.';
	writeln(prediccion[6,pred]);
end;
	
procedure virgo;
begin
	prediccion[7,0]:='Esta semana Mercurio decidira dejar a Geminis y al area del desarrollo profesional de tu horoscopo';
	prediccion[7,1]:='Durante esta semana, te conviene aprovechar sabiamente los ultimos dias de Mercurio para mantener el ritmo activo en tu trabajo y materializar ideas';
	prediccion[7,2]:='Los astros estan de tu lado y te ayudan en asuntos laborales, aprovechalo. Encontraras una solucion laboral que se adaptara muy bien a tus necesidades';
	prediccion[7,3]:='Te esperan unos dias un tanto duros en el trabajo, espera porque pronto pasaran. Tienes que tener paciencia, enseguida te vendra una buena racha, espera un poco';
	prediccion[7,4]:='Dedicate a poner al dia y en orden tus asuntos, los tienes un poco abandonados. Vas a tener novedades interesantes en el amor, quizas conozcas a alguien';
	writeln(prediccion[7,pred]);
end;

procedure  libra;
begin
	prediccion[8,0]:='Ten cuidado al hacer negocios con gente que no conoces bien Libra, piensatelo mucho';
	prediccion[8,1]:='En el trabajo te espera una etapa dura que traera recompensas en el futuro';
	prediccion[8,2]:='Si pones un poco mas de interes en la convivencia, resolveras los problemas de amor. Tienes que cuidarte mas para recuperar tu salud de siempre, piensa en ti.';
	prediccion[8,3]:='Tendras que hacer frente a algunos asuntos familiares, pero no muy preocupantes. Evita enfrentarte a la gente en las discusiones, di las cosas con delicadeza';
	prediccion[8,4]:='Te sientes con fuerzas y con ilusion para emprender cosas nuevas, aprovecha. Si tienes dolores de cabeza, es por acumular tensiones, relajate un poquito. ';
	writeln(prediccion[8,pred]);
end;
 
procedure escorpio;
begin
	prediccion[9,0]:='Si habías hecho una inversion Escorpio, todavia tendras que esperar un poco por los frutos. En las cuestiones de dinero no te conviene nada arriesgarte este dia';
	prediccion[9,1]:='Este es un buen momento para los temas sentimentales que debes aprovechar. Si tienes problemas de convivencia, resuelvelos con mas comunicacion';
	prediccion[9,2]:='Procura aislarte un poco para reflexionar, en el amor veras las cosas de otro color. Tienes que darle rienda suelta a tus emociones, pero hazlo de un modo calmado';
	prediccion[9,3]:='Iras recuperando tu buen humor segun pasen las horas y te sentiras mejor. Te vendria bien vigilar un poco tu salud, la tienes olvidada ultimamente';
	prediccion[9,4]:='Los astros te estan alterando un poco, pero si te lo propones lo controlaras. Te sientes con mucha actividad mentalmente y tendras grandes ideas, ponlas en practica.';
	writeln(prediccion[9,pred]);
end;

procedure sagitario;
begin
	prediccion[10,0]:='Haras unas compras que mermaran tu economia Sagitario, pero te podras sentir satisfecho. Tienes un buen momento para hacer cambios en tu vida de trabajo, piensalo';
	prediccion[10,1]:='Acabaras tus tareas antes de lo que piensas y tendras tiempo para ti. Si tienes prevision y no gastas de mas te ira muy bien, adelantate al futuro.';
	prediccion[10,2]:='Procura salir todo lo que puedas, tendras oportunidades de diversion aseguradas y puede que encuentres el amor. Si tienes dificultades, tu familia te ayudara encantada, pero debes decirselo';
	prediccion[10,3]:='Solo tienes que poner un poco mas de tu parte en tu relacion para que vayan las cosas bien. Te sientes bien, con alegria y buen humor, disfruta mucho de este dia';
	prediccion[10,4]:='Intenta preocuparte menos y disfruta mas de las cosas pequenas que tienes. Adopta una dieta mas acorde con tu ritmo de vida, tienes que pensar en ti.';
	writeln(prediccion[10,pred]);
end;

procedure capricornio;
begin
	prediccion[11,0]:='Las personas de tu alrededor Capricornio, haran que te sientas muy a gusto en el trabajo. Recuerda que hay un momento para cada cosa y ahora la profesion pesa mas.';
	prediccion[11,1]:='Haras tus tareas bien, pero si te esfuerzas puedes llegar mucho mas lejos. Tendras una actividad intensa y buenos resultados, nada podra detenerte';
	prediccion[11,2]:='Si tratas de no discutir con los demas hoy podras tener un dia magnifico.  En el amor, iniciaras una etapa de conquista y de seduccion que elevara mucho tu animo';
	prediccion[11,3]:='Conoceras a alguien que puede solucionarte asuntos gracias a unos amigos. Fisicamente te falta energia, pero en el plano mental te encuentras muy bien.';
	prediccion[11,4]:='Tienes la cabeza llena de preocupaciones por tu salud que en realidad no son para tanto, estás bien. Sigue enfocado en tus metas, trata de no distraerte. ';
	writeln(prediccion[11,pred]);
end;

BEGIN
    randomize;
	begin
	pred:=random(5);
	end;
	clrscr;
	repeat;
	Writeln('-------Bienvenido a JupiterChild--------');
	writeln;
	Writeln('El sitio donde descubriras lo que los');
	Writeln('Astros tienen preparado para tu vida.');
	Writeln;
	Writeln('----------------- Menu -----------------');writeln;
	writeln('1. Registro de Datos');
	Writeln('2. Lectura de Horoscopo');
	writeln('3. Salida');
	readln(opc);
	clrscr;
	begin
	Case opc of
	1: begin 
			Writeln('Introduzca su primer nombre:');
			read(nombre);
			writeln('Introduzca la fecha del dia en que nacio: ejemplo, 31');
			read(dia[0]);
			writeln('Introduzca el mes en el que nacio: ejemplo, 12');
			read(mes[0]);
			readkey;
			clrscr;
		end;
	2: Begin
	if ((dia[0]>=21) and (mes[0]=3)) or ((dia[0]<=20) and (mes[0]=4)) then
        begin
            writeln('Querid@ ',nombre);
			writeln('tu signo es:ARIES');
            aries;
            readkey;
			clrscr;
        end;
    if ((dia[0]=24) and (mes[0]=9)) or ((dia[0]<=23) and (mes[0]=10)) then
        begin
            writeln('Querid@ ',nombre);
			writeln('tu signo es:LIBRA');
            libra;
            readkey;
            clrscr;
        end;
    if ((dia[0]>=21) and (mes[0]=4)) or ((dia[0]<=21) and (mes[0]=5)) then
        begin
            writeln('Querid@ ',nombre);
			writeln('tu signo es:TAURO');
            tauro;
            readkey;
            clrscr;
        end;
    if ((dia[0]>=24) and (mes[0]=10)) or ((dia[0]<=22) and (mes[0]=11)) then
        begin
            writeln('Querid@ ',nombre);
			writeln('tu signo es:ESCORPIO');
            escorpio;
            readkey;
            clrscr;
        end;
    if ((dia[0]>=22) and (mes[0]=5)) or ((dia[0]<=21) and (mes[0]=6)) then
        begin
            writeln('Querid@ ',nombre);
			writeln('tu signo es:GEMINIS');
            geminis;
            readkey;
            clrscr;
        end;
    if ((dia[0]>=23) and (mes[0]=11)) or ((dia[0]<=21) and (mes[0]=12)) then
        begin
            writeln('Querid@ ',nombre);
			writeln('tu signo es:SAGITARIO');
            sagitario;
            readkey;
            clrscr;
        end;
    if ((dia[0]>=21) and (mes[0]=6)) or ((dia[0]<=23) and (mes[0]=7)) then
        begin
            writeln('Querid@ ',nombre);
			writeln('tu signo es:CANCER');
            cancer;
            readkey;
            clrscr;
        end;
    if ((dia[0]>=22) and (mes[0]=12)) or ((dia[0]<=20) and (mes[0]=1)) then
        begin
            writeln('Querid@ ',nombre);
			writeln('tu signo es:CAPRICORNIO');
            capricornio;
            readkey;
            clrscr;
        end;
    if ((dia[0]>=24) and (mes[0]=7)) or ((dia[0]<=23) and (mes[0]=8)) then
        begin
            writeln('Querid@ ',nombre);
			writeln('tu signo es:LEO');
            leo;
            readkey;
            clrscr;
        end;
    if ((dia[0]>=21) and (mes[0]=1)) or ((dia[0]<=19) and (mes[0]=2)) then
        begin
            writeln('Querid@ ',nombre);
			writeln('tu signo es:ACUARIO');
            acuario;
            readkey;
            clrscr;
        end;
    if ((dia[0]>=24) and (mes[0]=8)) or ((dia[0]<=23) and (mes[0]=9)) then
        begin
            writeln('Querid@ ',nombre);
			writeln('tu signo es:VIRGO');
            virgo;
            readkey;
            clrscr;
        end;
    if ((dia[0]>=20) and (mes[0]=2)) or ((dia[0]<=20) and (mes[0]=3)) then
        begin
           	writeln('Querid@ ',nombre);
	        writeln('tu signo es:PISCIS');
            piscis;
            readkey;
            clrscr;
            
        end;
			   End;
			3: begin
			Writeln('Gracias por participar en JupiterChild, Hasta Pronto!');
			end;
		end; 
		end;
			Until(opc=3);
				Readkey;
			
END.
